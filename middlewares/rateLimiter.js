const rateLimiter=require('express-rate-limit');
const authLimiter=rateLimiter({
    windowMs:15 * 60 * 1000,
    max:20
});
module.exports= {authLimiter};