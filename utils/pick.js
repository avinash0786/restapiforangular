//pick function is used to take out the provided keys value from the object
const pick=(object,keys)=>{
    return keys.reduce((obj,key)=>{
        if (object && Object.prototype.hasOwnProperty.call(object,key)){
            obj[key]=object[key];
        }
        return obj;
    },{})
}

module.exports=pick;