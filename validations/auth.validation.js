const joi=require('joi');
const {password}=require('./custom.validation');

const register={
    body:joi.object().keys({
        email:joi.string().required().email(),
        password:joi.string().required().custom(password),
        name:joi.string().required()
    })
}
const login={
    body: joi.object().keys({
        email:joi.string().required().email(),
        password:joi.string().required().custom(password)
    })
}
const resetPassword={
    query:joi.object().keys({
        token:joi.string().required()
    }),
    body:joi.object().keys({
        password:joi.string().required().custom(password)
    })
}
const logout={
    body:joi.object().keys({
        refreshToken:joi.string().required()
    })
}
const refreshToken={
    body:joi.object().keys({
        refreshToken:joi.string().required()
    })
}
const forgotPassword={
    body:joi.object().keys({
        email:joi.string().email().required()
    })
}
const verifyEmail={
    body:joi.object().keys({
        token:joi.string().required()
    })
}


module.exports={
    register,
    login,
    resetPassword,
    verifyEmail,
    forgotPassword,
    refreshToken,
    logout
}