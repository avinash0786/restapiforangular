const app = require('./app');
const mongoose=require('mongoose');
const config=require('./config/config');

let server;
mongoose.connect(config.mongoose.url,config.mongoose.options)
.then(()=>{
    server = app.listen(config.port, () => {
        console.log('DB connect success')
        console.info(`Listening to port ${config.port}`);
        console.log(`http://localhost:${config.port}`)
    });
})
const exitHandler = () => {
    if (server) {
        server.close(() => {
            console.info('Server closed');
            process.exit(1);
        });
    } else {
        process.exit(1);
    }
};

const unexpectedErrorHandler = (error) => {
    console.error(error);
    exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
    console.info('SIGTERM received');
    if (server) {
        server.close();
    }
});