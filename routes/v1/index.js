const express = require('express');
const authRoute=require('./auth.route');
const userRoute=require('./user.route');
const joi=require('joi');
const {password}=require('../../validations/custom.validation');

const router=express.Router();

//here we will define all our routes
const defaultRoute=[
    {path:'/auth',route:authRoute},
    {path:'/user',route:userRoute},
];

//by looping over all the routes we are registering all the routes to the router
defaultRoute.forEach((link)=>{
    router.use(link.path,link.route);
});

router.get('/test',((req, res) => {
    const schema=joi.object().keys({
        email:joi.string().required().email(),
        password:joi.string().required().custom(password)
    });
    const {value,error}=schema.validate({
        email:"avin@gamil.com",
        password:''
    });
    console.log(value)
    if (error){
        console.log("Validation error");
        console.log(error);
    }
    res.json({
        info:"V1 route main",
        valid:error
    })
}))

module.exports=router;