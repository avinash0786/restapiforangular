const joi=require("joi");
const path=require('path');
const dotenv=require('dotenv').config();

// dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema=joi.object()
.keys({
    NODE_ENV:joi.string().valid('production','development','test').required(),
    PORT:joi.number().default(3000),
    MONGODO_URL:joi.string().required().description('MongoDB connection URl'),
    JWT_SECRET:joi.string().required().description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES:joi.number().default(30).description('Minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS:joi.number().default(5).description('Days after which JWT refresh tokens expire'),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES:joi.number().default(10).description('minutes after which reset password token expires'),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES:joi.number().default(10).description('minutes after which verify email token expires'),
    SG_API_KEY:joi.string().required().description('Send gird mail api key')
})
.unknown();
//A property can be unpacked from an object and assigned to a variable with a different name than the object property
//here we are unpacking a value from the returned object a variable named 'value' is assinged to variable named 'envVars'

const {value:envVars,error}=envVarsSchema.preferences({errors: {label:'key'}}).validate(process.env);
if (error){
    throw new Error(`Config Validation error: ${error.message}`);
}

module.exports={
    env:envVars.NODE_ENV,
    port:envVars.PORT,
    mongoose:{
        url:envVars.MONGODO_URL,
        options:{
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    },
    jwt:{
        secret:envVars.JWT_SECRET,
        accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
        refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
        resetPasswordExpirationMinutes: envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
        verifyEmailExpirationMinutes:envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES
    },
    mail:{
        apiKey:envVars.SG_API_KEY
    }
}